---
title: "Statement on Rahul Gandhi Disqualification From Parliament and Conviction"
date: 2023-03-30T16:43:45+05:30
draft: false
---
Rahul Gandhi was [disqualified from Parliament](https://indianexpress.com/article/explained/explained-law/explained-law-rahuls-disqualification-after-8517591/) in a desperate move by the BJP government at the centre. There was a wave of support for Rahul Gandhi even from political parties who were not close to Congress till now like Trinamool Congress and Aam Aadmi Party. Even Pattali Makkal Katchi (PMK), an ally of the National Democratic Alliance (NDA) in Tamil Nadu said Rahul Gandhi’s punishment is disproportionate to his speech and that he [shouldn’t have been disqualified from the Lok Sabha in a haste](https://www.hindustantimes.com/india-news/rahul-should-not-have-been-disqualified-says-nda-ally-pmk-101679858030842.html). While standing up for Rahul Gandhi is an important step, we have to remember our actions have to go beyond responding to specific events. This will need building a united opposition by all citizens against this government.

We can no longer remain neutral and watch the politics unfold from the sidelines. Being neutral at this point will just enable Modi to comeback easily. Being divided at this point also helps Modi come back easily. So we have to realize what is at stake - our democracy and rights, and take steps necessary to bring down Modi from power in 2024, keeping our egos and differences aside, because fighting among ourselves will only help Modi.

Congress may not be an ideal party for many of us, but we can't get to an ideal government in a years time, so we will have to include Congress in an interim solution and get at least fascist and authoritarian features removed from the government. We can disagree with Congress and keep fighting for an even better government when at least a space for dissent and a reasonable democracy is restored.

As for Congress, Prem Shankar Jha [writes](https://thewire.in/politics/the-congress-has-to-recognise-that-its-enemys-enemy-is-its-friend) in The Wire, "if the party continues to dream of recovering lost glory on its own, and to regard every party that has ousted it from dominance anywhere in the country as its enemy, then the remaining  regional parties will be left with no option but to form a Third Front to fight Modi’s BJP".

We have [defeated](https://www.hindustantimes.com/columns/emergency-42-that-bizarre-experience-made-us-realise-the-courage-of-indians/story-T0A4rSyxlbw64qF9KLuXgK.html) 21 month long emergency (1975-1977) of Indira Gandhi as a united opposition and we can still do that again, if only we realize the danger our country is in, and come together setting aside our egos and differences.
