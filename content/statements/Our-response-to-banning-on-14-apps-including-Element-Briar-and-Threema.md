---
title: "Our Response to Banning on 14 Apps Including Element Briar and Threema"
date: 2023-05-15T19:11:35+05:30
draft: false
---
Update: <i>The ban has been challenged by petitioners Kiran and Praveen(also a permanent member of our group, Indian Pirates) from the <a href="https://fsci.in">Free Software Community of India</a> in the Kerala High Court assisted by lawyers from the [Software Freedom Law Center, India](https://sflc.in). </i>

The government of India recently banned 14 apps including [Free Software](https://www.gnu.org/philosophy/free-software-even-more-important.html) ones like Element and Briar [1]. According to media reports, there are two reasons for this ban.

1. These apps are used to provide information to terrorists.

2. These apps do not have a representative in India.

The free software apps mentioned above are distributed in nature. They do not have a central service provider but have thousands of servers spread across the world that are maintained by independent individuals and organisations. The government has banned the flagship clients for these services which causes no trouble to terrorists but causes inconvenience to the general public. Consider Element. It is only one of the several clients that connect with servers communicating with each other using the Matrix protocol. For a terrorist, it is just a matter of running their own Matrix server and switching to a different client. However, this is an inconvenience for the general public who may not have that much knowledge about the technicalities. Imagine the government banning iPhones because they find terrorists are using them. The terrorists may easily switch to an Android phone but the general public may find it very difficult to make the switch.

There is also the question of liablilty. Just like how Apple is not held liable for misuse of a mobile connection, element cannot be held liable for misuse of a matrix account. Even in case of a misuse, the matrix service providers are protected by Section 79 of the IT Act [2]. The govt has not even mentioned any intermediary here, let alone taking the steps of reporting the misuse. They seemed to have assumed all chat apps are also intermediaries. This is also similar to banning Google Chrome when they want to ban a website.

The government has also raised concerns about end to end encypted communication. It is worth noting what Element has to say about this in their statement [3] - "Some governments see undermining encryption as the most effective way to combat the ills of terrorism or other illegal behaviour. That approach is completely flawed; it just removes ordinary people’s ability to communicate in private which leaves them vulnerable to all types of surveillance, crime and subjugation.".

Free Software apps like Element and Briar are not threats to national securtity but key to our self-reliance. Even the govt have tall claims about Atmanirbhar Bharat. Actions like these are preventing us from achieving self-reliance and moving us to complete dependence on foreign companies and platforms.

Another stated reason for the ban is “These apps do not have any representatives in India and cannot be contacted for seeking information as mandated by the Indian laws,” [4] However, as per the law, this requirement is only for significant intermediaries with at least 50 Lakh users. We feel the move is aimed at taking away our fundamental right to privacy[6] by banning apps that don't allow surveillance by the govt, by using terrorists as the boogeyman. The same government has earlier argued in the Supreme Court that citizens do not have fundamental right to privacy[7]. We, the people of India, have to understand the real intentions and stand up for our rights, educate people about importance of privacy and practical steps they can take to achieve it. We hope the Supreme Court will stop this attack on our right to privacy.

Security expert Bruce Schenier has a good article for further reading.[8]

[1] https://www.medianama.com/2023/05/223-make-order-banning-14-apps-jammu-and-kashmir-public-sflc-in/

[2] https://www.livemint.com/technology/tech-news/what-are-india-s-intermediary-protection-rules-11623951065544.html

[3] https://element.io/blog/india-bans-flagship-client-for-the-matrix-network/

[4] https://indianexpress.com/article/india/mobile-apps-blocked-jammu-kashmir-terrorists-8585046/

[5] https://www.gnu.org/philosophy/free-software-even-more-important.html

[6] https://thewire.in/law/supreme-court-aadhaar-right-to-privacy

[7] https://www.hindustantimes.com/india/citizens-do-not-have-fundamental-right-to-privacy-centre-tells-sc/story-ykRepEFYCvWteceqLNuz9O.html

[8] https://www.schneier.com/essays/archives/2009/01/terrorists_may_use_g.html
